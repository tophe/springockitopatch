package org.kubek2k.mockito.spring.factory;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.kubek2k.springockito.annotations.internal.ResettableMock;
import org.mockito.Mockito;
import org.mockito.internal.creation.MockSettingsImpl;
import org.mockito.internal.creation.settings.CreationSettings;
import org.mockito.internal.util.MockCreationValidator;
import org.mockito.internal.util.MockNameImpl;
import org.mockito.internal.util.MockitoSpy;
import org.mockito.mock.MockCreationSettings;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.FactoryBean;

public class SpyFactoryBean<T> implements FactoryBean<T>, ResettableMock {

    private T wrappedInstance;

    private T spyInstance;

    public SpyFactoryBean(T wrappedInstance) {
        this.wrappedInstance = wrappedInstance;
    }

    public T getObject() throws Exception {
    	if (spyInstance != null) {
			return spyInstance;
		}
		if (AopUtils.isAopProxy(wrappedInstance)
				&& wrappedInstance instanceof Advised) {
				spyInstance = (T) Mockito.mock(((Advised) wrappedInstance)
						.getTargetSource().getTargetClass(),new AopMockSettings().spiedInstance(wrappedInstance).defaultAnswer(Mockito.CALLS_REAL_METHODS));
			} else {
				spyInstance = (T) Mockito.spy( wrappedInstance);
			}
		return spyInstance;
    }

    @SuppressWarnings("unchecked")
    public Class<? extends T> getObjectType() {
        return (Class<? extends T>) wrappedInstance.getClass();
    }

    public boolean isSingleton() {
        return true;
    }

    public void resetMock() {
        T object = null;
        try {
            object = getObject();
            Mockito.reset(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @SuppressWarnings("serial")
	static class AopMockSettings<T> extends MockSettingsImpl<T>{
			
			public MockCreationSettings<T> confirm(Class<T> typeToMock) {
		        return validatedSettings(typeToMock, this);
		    }
			   private static <T> CreationSettings<T> validatedSettings(Class<T> typeToMock, CreationSettings<T> source) {
			        MockCreationValidator validator = new MockCreationValidator();
			        if (typeToMock!=null){
			        	validator.validateType(typeToMock);
			        	validator.validateExtraInterfaces(typeToMock, source.getExtraInterfaces());}
			        
			        CreationSettings<T> settings = new CreationSettings<T>(source);
			        if (typeToMock!=null){
			        	settings.setMockName(new MockNameImpl(source.getName(), typeToMock));}
			        
			        else{settings.setMockName(new MockNameImpl(source.getName()));}
			        settings.setTypeToMock(typeToMock);
			        settings.setExtraInterfaces(prepareExtraInterfaces(source));
			        return settings;
			    }
			   
		    @SuppressWarnings("unchecked")
			private static Set<Class> prepareExtraInterfaces(CreationSettings settings) {
		        Set<Class> interfaces = new HashSet<Class>(settings.getExtraInterfaces());
		        if(settings.isSerializable()) {
		            interfaces.add(Serializable.class);
		        }
		        if (settings.getSpiedInstance() != null) {
		            interfaces.add(MockitoSpy.class);
		        }
		        return interfaces;
		    }
		
	}

}
