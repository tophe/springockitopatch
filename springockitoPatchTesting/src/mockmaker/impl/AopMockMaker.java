package mockmaker.impl;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;
import org.mockito.internal.creation.settings.CreationSettings;
import org.mockito.invocation.MockHandler;
import org.mockito.mock.MockCreationSettings;
import org.mockito.plugins.MockMaker;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.framework.AopInfrastructureBean;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.util.ClassUtils;

/**
 * Class AopMockMaker.
 * @author tophe
 */
public final class AopMockMaker implements MockMaker {

    /** The proxy factory. */
    private ProxyFactory proxyFactory;

    /** Logger de la classe. */
    private static final Logger LOGGER = Logger.getLogger(AopMockMaker.class);

    /*
     * (non-Javadoc)
     * @see org.mockito.plugins.MockMaker#createMock(org.mockito.mock.MockCreationSettings,
     * org.mockito.invocation.MockHandler)
     */
    @SuppressWarnings("unchecked")
    public <T> T createMock(final MockCreationSettings<T> pSettings, final MockHandler pHandler) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("AopMock with settings:" + pSettings.getClass());
        }

        final InvocationHandlerAdapter advice = new InvocationHandlerAdapter(pHandler);
        proxyFactory = new ProxyFactory();
        proxyFactory.addAdvice(advice);
        proxyFactory.setProxyTargetClass(true);

        final Class beanType = pSettings.getTypeToMock();
        if (beanType != null) {
            proxyFactory.setTargetClass(beanType);
            proxyFactory.setInterfaces(ClassUtils.getAllInterfacesForClass(beanType,
                    ClassUtils.getDefaultClassLoader()));
        }

        proxyFactory.addInterface(AopInfrastructureBean.class);

        final T spiedInstance = (T) pSettings.getSpiedInstance();
        boolean isAdvised = false;

        if (spiedInstance != null) {
            isAdvised = AopUtils.isAopProxy(spiedInstance) && spiedInstance instanceof Advised;

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("spiedInstance is present:"+spiedInstance.getClass());
            }
            
            if (!isAdvised) {
                
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("spiedInstance is not AopProxy:"+spiedInstance.getClass());
                }
                proxyFactory.setTarget(spiedInstance);
                

            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("spiedInstance is AopProxy:"+spiedInstance.getClass()+",cglib?:"+AopUtils.isCglibProxy(spiedInstance)+",JdkDynamicProxy?:"+AopUtils.isJdkDynamicProxy(spiedInstance));
                }
                proxyFactory.setProxyTargetClass(AopUtils.isCglibProxy(spiedInstance));
                proxyFactory.setTargetSource(((Advised) spiedInstance).getTargetSource());

                if (pSettings instanceof CreationSettings) {

                    try {
                        final Field fieldSpiedInstance = CreationSettings.class.getDeclaredField("spiedInstance");
                        fieldSpiedInstance.setAccessible(true);
                        fieldSpiedInstance.set(pSettings, null);
                    } catch (final Exception e) {
                        LOGGER.error("Impossible de reinitialiser la target du proxy !");
                    }
                }

                for (Class<?> classInterface : ((Advised) spiedInstance).getProxiedInterfaces()) {
                    proxyFactory.addInterface(classInterface);
                }

                for (Advisor advisor : ((Advised) spiedInstance).getAdvisors()) {
                    proxyFactory.addAdvisor(advisor);
                }
            }
        }
        else{
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("no spiedInstance!");
            }
        }

        final T proxyMock = (T) proxyFactory.getProxy();
        advice.setProxyMock(proxyMock);

        return (T) proxyMock;
    }

    /*
     * (non-Javadoc)
     * @see org.mockito.plugins.MockMaker#resetMock(java.lang.Object, org.mockito.invocation.MockHandler,
     * org.mockito.mock.MockCreationSettings)
     */
    @SuppressWarnings("unchecked")
    public void resetMock(final Object pMock, final MockHandler pNewHandler, final MockCreationSettings pSettings) {
        final Advisor[] advisors = ((Advised) pMock).getAdvisors();

        for (Advisor advisor : advisors) {

            if (advisor.getAdvice() instanceof InvocationHandlerAdapter) {
                ((InvocationHandlerAdapter) advisor.getAdvice()).setHandler(pNewHandler);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see org.mockito.plugins.MockMaker#getHandler(java.lang.Object)
     */
    public MockHandler getHandler(final Object pMock) {

        if (!(pMock instanceof Advised)) {
            return null;
        }

        final Advisor[] advisors = ((Advised) pMock).getAdvisors();

        for (Advisor advisor : advisors) {

            if (advisor.getAdvice() instanceof InvocationHandlerAdapter) {
                return ((InvocationHandlerAdapter) advisor.getAdvice()).getHandler();
            }
        }
        return null;
    }
}
