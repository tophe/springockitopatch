package mockmaker.impl;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.mockito.internal.invocation.InvocationImpl;
import org.mockito.internal.invocation.MockitoMethod;
import org.mockito.internal.invocation.realmethod.RealMethod;
import org.mockito.internal.progress.SequenceNumber;
import org.mockito.internal.util.ObjectMethodsGuru;
import org.mockito.invocation.MockHandler;
import org.mockito.mock.MockCreationSettings;

/**
 * Class InvocationHandlerAdapter.
 * @author tophe
 */
final class InvocationHandlerAdapter implements MethodInterceptor {

    /** Logger de la classe. */
    private static final Logger LOGGER = Logger.getLogger(AopMockMaker.class);

    
    /** The handler. */
    private MockHandler handler;

    /** The object methods guru. */
    private final ObjectMethodsGuru objectMethodsGuru = new ObjectMethodsGuru();

    /** The proxy mock. */
    private Object proxyMock;

    /**
     * Sets the proxy mock.
     * @param pProxyMock the new proxy mock
     */
    public void setProxyMock(final Object pProxyMock) {
        this.proxyMock = pProxyMock;
    }

    /**
     * Instantiates a new invocation handler adapter.
     * @param pHandler the handler
     */
    public InvocationHandlerAdapter(final MockHandler pHandler) {
        this.handler = pHandler;
    }

    /*
     * (non-Javadoc)
     * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
     */
    @Override
    public Object invoke(final MethodInvocation pMethodInvocation) throws Throwable {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("aop invoke method:" + pMethodInvocation+",args:"+Arrays.toString(pMethodInvocation.getArguments()));
        }
        
        if (objectMethodsGuru.isEqualsMethod(pMethodInvocation.getMethod())) {
            return pMethodInvocation.getThis() == pMethodInvocation.getArguments()[0];
        } else if (objectMethodsGuru.isHashCodeMethod(pMethodInvocation.getMethod())) {
            return System.identityHashCode(proxyMock);
        }

        if (pMethodInvocation.getArguments() == null) {
            throw new IllegalArgumentException();
        }

        final ProxiedMethod proxiedMethod = new ProxiedMethod(pMethodInvocation);
        return handler.handle(new AopInvocationImpl(proxyMock, proxiedMethod, pMethodInvocation.getArguments(),
                SequenceNumber.next(), proxiedMethod));

    }

    /**
     * Gets the handler.
     * @return the handler
     */
    public MockHandler getHandler() {
        return handler;
    }

    /**
     * Sets the handler.
     * @param pHandler the new handler
     */
    public void setHandler(final MockHandler pHandler) {
        this.handler = pHandler;
    }

    /**
     * Class AopInvocationImpl.
     * @author tophe
     */
    @SuppressWarnings("serial")
    static class AopInvocationImpl extends InvocationImpl {

        /** The real method. */
        private final RealMethod realMethod;

        /**
         * Instantiates a new aop invocation impl.
         * @param pMock the mock
         * @param pMockitoMethod the mockito method
         * @param pArgs the args
         * @param pSequenceNumber the sequence number
         * @param pRealMethod the real method
         */
        public AopInvocationImpl(final Object pMock, final MockitoMethod pMockitoMethod, final Object[] pArgs,
                final int pSequenceNumber, final RealMethod pRealMethod) {

            super(pMock, pMockitoMethod, pArgs, pSequenceNumber, pRealMethod);

            this.realMethod = pRealMethod;
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.InvocationImpl#callRealMethod()
         */
        public Object callRealMethod() throws Throwable {
            return this.realMethod.invoke(this.getMock(), this.getRawArguments());
        }

    }

    /**
     * Class ProxiedMethod.
     * @author tophe
     */
    private static class ProxiedMethod implements MockitoMethod, RealMethod {

        /** The method. */
        private Method method;

        /** The method invocation. */
        private final MethodInvocation methodInvocation;

        /**
         * Instantiates a new proxied method.
         * @param pMethodInvocation the method invocation
         */
        public ProxiedMethod(final MethodInvocation pMethodInvocation) {
            this.method = pMethodInvocation.getMethod();
            this.methodInvocation = pMethodInvocation;
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.MockitoMethod#getName()
         */
        public String getName() {
            return method.getName();
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.MockitoMethod#getReturnType()
         */
        public Class<?> getReturnType() {
            return method.getReturnType();
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.MockitoMethod#getParameterTypes()
         */
        public Class<?>[] getParameterTypes() {
            return method.getParameterTypes();
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.MockitoMethod#getExceptionTypes()
         */
        public Class<?>[] getExceptionTypes() {
            return method.getExceptionTypes();
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.MockitoMethod#isVarArgs()
         */
        public boolean isVarArgs() {
            return method.isVarArgs();
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.MockitoMethod#getJavaMethod()
         */
        public Method getJavaMethod() {
            return method;
        }

        /*
         * (non-Javadoc)
         * @see org.mockito.internal.invocation.realmethod.RealMethod#invoke(java.lang.Object, java.lang.Object[])
         */
        public Object invoke(final Object pTarget, final Object[] pArguments) throws Throwable {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("invoke method:" + methodInvocation);
            }
            return methodInvocation.proceed();
        }
    }

}
