/*
 * Copyright 2006-2007 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package junit;

import static org.mockito.Mockito.doAnswer;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.kubek2k.springockito.annotations.WrapWithSpy;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import bean.ITestBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/springConfig/springockitoNamespace.xml" }, loader = SpringockitoContextLoader.class)
public class SpringockitoNamespaceTest {

    @Resource(name = "jdkProxyBeanSpy")
    private ITestBean jdkProxyBeanSpy;

    @Resource(name = "cglibProxyBeanSpy")
    private ITestBean cglibProxyBeanSpy;
    
    @Resource(name = "beanSpy")
    private ITestBean beanSpy;

    @Resource(name = "jdkProxyBeanMock")
    private ITestBean jdkProxyBeanMock;

    @Resource(name = "cglibProxyBeanMock")
    private ITestBean cglibProxyBeanMock;
    
    @Resource(name = "beanMock")
    private ITestBean beanMock;

    
      static class SpyTestAnswer implements Answer<Object> {

        private String logPrefix;
        
        public SpyTestAnswer(String logPrefix) {
            super();
            this.logPrefix = logPrefix;
        }

        @Override
        public Object answer(InvocationOnMock invocation) throws Throwable {
            try {
                return logPrefix + invocation.callRealMethod();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }
    
     static class MockTestAnswer implements Answer<Object> {

        private String logPrefix;
        
        public MockTestAnswer(String logPrefix) {
            super();
            this.logPrefix = logPrefix;
        }

        @Override
        public Object answer(InvocationOnMock invocation) throws Throwable {
            try {
                return logPrefix + invocation.getArguments()[0];
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }
    

    @Before
    public void createStubs() throws Exception {

        doAnswer(new SpyTestAnswer("spyHasProcessedOnJdkProxyBean,"))           
        .when(jdkProxyBeanSpy).logMe(Mockito.anyString());

        doAnswer(new SpyTestAnswer("spykHasProcessedOnCglibProxyBean,"))           
        .when(cglibProxyBeanSpy).logMe(Mockito.anyString());

        
        doAnswer(new SpyTestAnswer("spykHasProcessedOnBean,"))           
        .when(beanSpy).logMe(Mockito.anyString());

        doAnswer(new MockTestAnswer("mockHasProcessedOnJdkProxyBean,"))           
        .when(jdkProxyBeanMock).logMe(Mockito.anyString());

        doAnswer(new MockTestAnswer("mockHasProcessedOnCglibProxyBean,"))           
        .when(cglibProxyBeanMock).logMe(Mockito.anyString());
        
        doAnswer(new MockTestAnswer("mockkHasProcessedOnBean,"))           
        .when(beanMock).logMe(Mockito.anyString());


    }

    
    @Test
    public void testSpy() throws Exception {
        System.out.println(jdkProxyBeanSpy.logMe("logMyJdkProxyTest"));
        System.out.println(cglibProxyBeanSpy.logMe("logMyCglibProxyTest"));
        System.out.println(beanSpy.logMe("logMyNoProxyTest"));
    }

    @Test
    public void testMock() throws Exception {
        System.out.println(jdkProxyBeanMock.logMe("logMyJdkProxyTest"));
        System.out.println(cglibProxyBeanMock.logMe("logMyCglibProxyTest"));
        System.out.println(beanMock.logMe("logMyNoProxyTest"));
    }

    
}